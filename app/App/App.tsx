import * as React from 'react';
import { Provider } from 'react-redux';

import Routes from '../Routes';
import store from '../Store/Store';

export default () =>
    (<Provider store={store}>
     <Routes />
    </Provider>);
