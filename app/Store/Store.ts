import { createStore, applyMiddleware } from 'redux';

import createSagaMiddleWare from 'redux-saga';
import mySaga from '../sagas';

import rootReducer from '../Reducers/index';

const sagaMiddleware: any = createSagaMiddleWare();

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(mySaga);

export default store;
