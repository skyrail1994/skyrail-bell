export const login = async (payload:any) => {
try {
    const response = await fetch('http://localhost:3001/auth', {
    method: 'POST',
    body: payload,
});
    return {
        isError: false,
        response,
    };
} catch (e) {
    return {
        isError: true
    };
  }
};