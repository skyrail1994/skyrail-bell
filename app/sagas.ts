import { call, takeLatest } from 'redux-saga/effects';

import { REQUEST_LOGIN } from './Action/Actions';
import { login } from './api';

function* loginSaga({ payload: {fields, callback} }) {
    const { isError } = yield call(login, fields);
    if (!isError) {
        history.push('/');
    }
}

export default function mySaga(): any {
   takeLatest(REQUEST_LOGIN, loginSaga);
}