export const REQUEST_LOGIN = 'REQUEST_LOGIN';

export const requestLogin = (payload:any) => ({
  type: REQUEST_LOGIN,
  payload,
});