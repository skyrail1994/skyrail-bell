import { combineReducers } from 'redux';

const rootReducer = combineReducers({dummy: (s:any = []) => s});

export default rootReducer;