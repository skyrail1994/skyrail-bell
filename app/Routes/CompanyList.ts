import * as React from 'react';
import axios from 'axios';


export default class CompanyList extends React.Component {
    state = {
        persons: '',
    };

    componentDidMount() {
        axios.get('http://www.mocky.io/v2/5c5932e6320000081bba36b9').then(res => {
            console.log(res);
                this.setState({ 
                    company: res.data 
                })
            });
    }

    render() {
        return (
            <ul>
            { this.state.companies.map(company => (
                <li key={company.id}>{company.name}</li>
            ))}

            </ul>
        );
    }
}