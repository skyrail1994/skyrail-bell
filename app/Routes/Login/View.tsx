import * as React from 'react';
import Form from '../../Components/Form';

export default (requestLogin: any, history: any ) => 
(<Form 
    onSubmit={(fields: any) =>
         requestLogin({ 
             fields, 
             callback: () => {
                 console.log('откат вызван!');
                 history.push('/');
                },
             })}
             />);
