import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';

import View from './View';
import { requestLogin } from '../../Action/Actions'

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = (dispatch: any) => 
bindActionCreators(
    {
    requestLogin,
},
 dispatch,
 );

export default connect(mapStateToProps, mapDispatchToProps)(View);
