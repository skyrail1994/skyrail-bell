import * as React from 'react';

export default class Form extends React.Component <any, any> {
    state = {
        username: '',
        password: '',
    };

    change = (e: any) => {
        this.setState({
            [e.taget.name]: e.taget.value,
        });
    };

    onSubmit = (e: any) => {
        e.preventDefault();
        if (this.props.onSubmit) {
            this.props.onSubmit(this.state);
        }
        this.setState({
            username: '',
            password: '',
        });
    };

    render() {
        return (
            <form>
                <input
                    name="username"
                    placeholder="Username"
                    value={this.state.username}
                    onChange={this.change}
                />
                <br />
                <input
                    name="password"
                    type="password"
                    placeholder="Password"
                    value={this.state.password}
                    onChange={this.change}
                />
                <br />
                <button onClick={this.onSubmit}>Submit</button>
            </form>
        );
    }
}